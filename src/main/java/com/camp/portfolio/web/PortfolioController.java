package com.camp.portfolio.web;

import com.camp.portfolio.service.PortfolioService;
//import org.springframework.beans.factory.annotation.Autowired;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@RequiredArgsConstructor
public class PortfolioController {

    //old
//    @Autowired
//    private PortfolioService portfolioService;

    private final PortfolioService portfolioService;


    @GetMapping("/")
    public  String index(Model model){
        model.addAttribute("portfolio", portfolioService.getPortfolio());
        return "index";
    }

}
