package com.camp.portfolio.service;

import com.camp.portfolio.dao.PorfolioDao;
import com.camp.portfolio.model.Portfolio;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PortfolioService {

    private final PorfolioDao porfolioDao;

    public Portfolio getPortfolio(){
//        return porfolioDao.
        return Portfolio.builder()
                .firstName("Veil")
                .lastName("Cusi")
                .age(25)
                .jobTitle("Software Engineer")
                .email("veil.cusi@gmail.com")
                .skype("myskype")
                .phone("09065521276")
                .address("Roxas Oriental Mindoro")
                .build();
    }
}
