package com.camp.portfolio.dao;

import com.camp.portfolio.model.Portfolio;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PorfolioDao extends JpaRepository<Portfolio, Long> {

}
